{even}
{title:Tests de régression}
{subtitle:guitartex}
{size:large}

[Ab]Ab, [Abm]Abm, [Ab7]Ab7, [Abmaj7]AbMaj7, [A#]A#, [A#m]A#m, [A#m/Bb]A#m/Bb, [A#maj7]A#Maj7, [Amaj7/Fb]Amaj7/Fb

[A6]A majeur 6

[EmMaj7] E mineur majeur 7

[Ab13]Ab13, [Ab13/Bb]Ab13/Bb, [Abm13/Bb]Abm13/Bb, [Cm13]Cm13

[A7/Bb]A7/Bb, [A7/C#]A7/C#, [A6]A6, [A6/Bb]A6/Bb [A9/Bb]A9/Bb

[Adim]Adim, [Adim/Bb]Adim/Bb, [Adim/C#]Adim/C#, [Adim7/C#]Adim7/C#

Phrase avec 'dim' et 'sus' et 'add' en majuscule

[Asus4]Accord [Asus4]suspendu [Asus4]4e

[Asus4/C#]Accord [Asus4/C#]suspendu [Asus4/C#]4e avec note basse

[Dm7b5]Accord [Dm7b5/Bb]demi-    diminué[Dm7b5/C#] (half-diminished)

[Dmb5]Accord [Dmb5/Bb]diminué [Dmb5/C#](diminished)
[A7b5]Accord [A7b5/Bb]7e [A7b5/C#]bémol 5
{np}

[A7sus4]Accord... [A7sus4/Bb]7e ................ [A7sus4/C#]sus4.............. [A9sus4]Accord 9e sus4

[A7add9]A7add9, [A7add9/Bb]A7add9/Bb, [A7add9/C#]A7add9/C#

[G9b]G9b, [A7b]A7b

[D7#11]D 7 #11

[C7b9#11]C7b9#11

[F5+]F5+, [F9+]F9+, [Fm9+]Fm9+

[Csus4sus2]

[Dm7#5/C#]

[E7#5]
