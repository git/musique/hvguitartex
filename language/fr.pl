# Menus
$name_menu_file = "File";
$name_menu_new = "New";
$name_menu_open = "Open...";
$name_menu_save = "Save";
$name_menu_saveas = "Save as...";
$name_menu_export = "Export";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Quit";

$name_menu_edit = "Edit";
$name_menu_paste1 = "Paste from Clipboard";
$name_menu_insert = "Insert file...";
$name_menu_crd = "crd -> chopro";
$name_menu_cut = "Cut: use ctrl-x";
$name_menu_copy = "Copy: use ctrl-c";
$name_menu_paste2 = "Paste: use ctrl-v";

$name_menu_directives = "Directives";
$name_menu_startchorus = "Start Chorus";
$name_menu_endchorus = "End Chorus";
$name_menu_startbridge = "Start Bridge";
$name_menu_endbridge = "End Bridge";
$name_menu_starttab = "Start Tablature";
$name_menu_endtab = "End Tablature";
$name_menu_startinstr = "Start Instrumental";
$name_menu_endinstr = "End Instrumental";
$name_menu_guitartab = "GuitarTab";
$name_menu_basstab = "BassTab";
$name_menu_title = "Title";
$name_menu_subtitle = "Subtitle";
$name_menu_newpage = "New Page";
$name_menu_comment = "Comment";
$name_menu_margin = "Margin Par";
$name_menu_second = "Second Voice";

$name_menu_songbook = "Songbook";
$name_menu_book = "Document class book";
$name_menu_booktitle = "Book title";
$name_menu_bookauthor = "Book author";
$name_menu_bookdate = "Book date";
$name_menu_chapter = "Chapter";
$name_menu_include = "Include";
$name_menu_even = "Even";

$name_menu_color = "Colors";
$name_menu_colorchorus = "Chorus";
$name_menu_colorbridge = "Bridge";
$name_menu_colortab = "Tablature";
$name_menu_colorinstr = "Instrumental";
$name_menu_second = "Second Voice";
$name_menu_secondback = "Background second Voice";

$name_menu_transpose = "Transpose";
$name_menu_transup = "Up";
$name_menu_transdown = "Down";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Help";
$name_menu_about = "about";

#Icons
$name_icon_chords = "Chords:";
$name_icon_tab = "Tablature:";
$name_icon_guitar = "Guitar";
$name_icon_bass = "Bass";
$name_icon_fret = " Fret:";
$name_icon_string = " String:";

$name_note_B = "Si";

# File Dialogs
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Text";
$name_all_files = "All Files";
$name_ask_save = "Do you want to save\nthe current document?";
$name_tex_ready = "TeX file ready";
$name_ps_ready = "Postscript file ready";
$name_pdf_ready = "PDF file ready";

# Common
$name_yes ="yes";
$name_no = "no";
$name_ok = "ok";
$name_cancel = "cancel";

$message_about = "In case of problems\nsee doc.pdf :-)\nor contact the author at\nguitartex-users\@lists.sourceforge.net";

# Tool Tips
$balloon_new = "Start a new document";
$balloon_open = "Open an existing document";
$balloon_save = "Save the current document";
$balloon_latex = "Export LaTeX file";
$balloon_ps = "Export postscript file";
$balloon_pdf = "Export PDF file";
$balloon_transpose_up = "Transpose up";
$balloon_transpose_down = "Transpose down";
$balloon_highlight = "Syntax highlighting";
$balloon_chord = "Insert chord";
$balloon_guitartab = "Insert guitar tablature";
$balloon_basstab = "Insert bass tablature";
$balloon_fret = "Enter fret number\nand click on string button\nto enter tablature notes";

# Templates
@name_template = ("empty", "Song", "Songbook");
