# Menus
$name_menu_file = "Datei";
$name_menu_new = "Neu";
$name_menu_open = "�ffnen...";
$name_menu_save = "Speichern";
$name_menu_saveas = "Speichern als...";
$name_menu_export = "Exportieren";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Beenden";

$name_menu_edit = "Bearbeiten";
$name_menu_paste1 = "Einf�gen aus Zwischenablage";
$name_menu_insert = "Datei einf�gen...";
$name_menu_crd = "crd -> chopro";
$name_menu_cut = "Auschneiden: verwende strg-x";
$name_menu_copy = "Kopieren: verwende strg-c";
$name_menu_paste2 = "Einf�gen: verwende strg-v";

$name_menu_directives = "Direktiven";
$name_menu_startchorus = "Anfang Refrain";
$name_menu_endchorus = "Ende Refrain";
$name_menu_startbridge = "Anfang Bridge";
$name_menu_endbridge = "Ende Bridge";
$name_menu_starttab = "Anfang Tabulatur";
$name_menu_endtab = "Ende Tabulatur";
$name_menu_startinstr = "Anfang Instrumental";
$name_menu_endinstr = "Ende Instrumental";
$name_menu_guitartab = "GuitarTab";
$name_menu_basstab = "BassTab";
$name_menu_title = "Titel";
$name_menu_subtitle = "Untertitel";
$name_menu_newpage = "Neue Seite";
$name_menu_comment = "Kommentar";
$name_menu_margin = "Randbemerkung";
$name_menu_second = "Zweite Stimme";

$name_menu_songbook = "Liederbuch";
$name_menu_book = "Dokumentklasse Buch";
$name_menu_booktitle = "Buchtitel";
$name_menu_bookauthor = "Buchautor";
$name_menu_bookdate = "Buchdatum";
$name_menu_chapter = "Kapitel";
$name_menu_include = "Einf�gen";
$name_menu_even = "Gerade Seite";

$name_menu_color = "Farben";
$name_menu_colorchorus = "Refrain";
$name_menu_colorbridge = "Bridge";
$name_menu_colortab = "Tabulatur";
$name_menu_colorinstr = "Instrumental";
$name_menu_second = "Zweite Stimme";
$name_menu_secondback = "Hintergrund zweite Stimme";

$name_menu_transpose = "Transponieren";
$name_menu_transup = "Hoch";
$name_menu_transdown = "Runter";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Hilfe";
$name_menu_about = "�ber";

#Icons
$name_icon_chords = "Akkorde:";
$name_icon_tab = "Tabulatur:";
$name_icon_guitar = "Gitarre";
$name_icon_bass = "Bass";
$name_icon_fret = " Bund:";
$name_icon_string = " Saite:";

$name_note_B = "H";

# Datei-Dialoge
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Text";
$name_all_files = "Alle Dateien";
$name_ask_save = "Wollen Sie das aktuelle\nDokument speichern?";
$name_tex_ready = "TeX-Datei fertig";
$name_ps_ready = "Postscript-Datei fertig";
$name_pdf_ready = "PDF-Datei fertig";

# Allgemein
$name_yes ="ja";
$name_no = "nein";
$name_ok = "ok";
$name_cancel = "cancel";

$message_about = "Bei Problemen\nsiehe doc.pdf :-)\noder schreiben Sie an\nguitartex-users\@lists.sourceforge.net";

# Tool Tips
$balloon_new = "Neues Dokument beginnen";
$balloon_open = "Vorhandenes Dokument �ffnen";
$balloon_save = "Aktuelles Dokument speichern";
$balloon_latex = "Als LaTeX-Datei exportieren";
$balloon_ps = "Als Postscript-Datei exportieren";
$balloon_pdf = "Als PDF-Datei exportieren";
$balloon_transpose_up = "hoch transponieren";
$balloon_transpose_down = "runter transponieren";
$balloon_highlight = "Syntax highlighting";
$balloon_chord = "Akkord einf�gen";
$balloon_guitartab = "Tabulatur f�r Gitarre einf�gen";
$balloon_basstab = "Tabulatur f�r Bass einf�gen";
$balloon_fret = "Um Tabulatur-Noten einzugeben,\nNummer des Bundes eingeben\nund auf den Button f�r die Saite klicken";

# Templates
@name_template = ("empty", "Song", "Songbook");
