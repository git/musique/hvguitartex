# Menus
$name_menu_file = "Archivo";
$name_menu_new = "Nuevo";
$name_menu_open = "Abrir...";
$name_menu_save = "Guardar";
$name_menu_saveas = "Guardar como...";
$name_menu_export = "Exportar";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Salir";

$name_menu_edit = "Modificar";
$name_menu_paste1 = "Pegar desde Clipboard";
$name_menu_insert = "Introducir archivo...";
$name_menu_crd = "crd -> chordpro";
$name_menu_cut = "Cortar: usa ctrl-x";
$name_menu_copy = "Copiar: usa ctrl-c";
$name_menu_paste2 = "Pegar: usa ctrl-v";

$name_menu_directives = "Directivas";
$name_menu_startchorus = "Inicio Coro";
$name_menu_endchorus = "Fin Coro";
$name_menu_startbridge = "Inicio Bridge";
$name_menu_endbridge = "Fin Bridge";
$name_menu_starttab = "Inicio Tabulado";
$name_menu_endtab = "Fin Tabulado";
$name_menu_startinstr = "Inicio Instrumental";
$name_menu_endinstr = "Fin Instrumental";
$name_menu_guitartab = "Tabulado para Guitarra";
$name_menu_basstab = "Tabulado para Bajo";
$name_menu_title = "T�tulo";
$name_menu_subtitle = "Subt�tulo";
$name_menu_newpage = "Nueva P�gina";
$name_menu_comment = "Comentario";
$name_menu_margin = "Parrafo a Margen";
$name_menu_second = "Segunda Voz";

$name_menu_songbook = "Songbook";
$name_menu_book = "Document class book";
$name_menu_booktitle = "T�tulo";
$name_menu_bookauthor = "Autor";
$name_menu_bookdate = "Fecha";
$name_menu_chapter = "Cap�tulo";
$name_menu_include = "Incluir archivo";
$name_menu_even = "Par";

$name_menu_color = "Colores";
$name_menu_colorchorus = "Acorde";
$name_menu_colorbridge = "Puente";
$name_menu_colortab = "Tabulado";
$name_menu_colorinstr = "Instrumental";
$name_menu_second = "Segunda Voz";
$name_menu_secondback = "Segunda voz en segundo plano";

$name_menu_transpose = "Transporta";
$name_menu_transup = "Arriba";
$name_menu_transdown = "Abajo";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Ayuda";
$name_menu_about = "Acerca de ";

#Icons
$name_icon_chords = "Acordes:";
$name_icon_tab = "Tabulado:";
$name_icon_guitar = "Guitarra";
$name_icon_bass = "Bajo";
$name_icon_fret = " Traste:";
$name_icon_string = " Cuerda:";

$name_note_B = "B";

# File Dialogs
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Text";
$name_all_files = "Todos los archivos";
$name_ask_save = "�Desea guardar el\narchivo actual?";
$name_tex_ready = "Archivo TeX listo";
$name_ps_ready = "Archivo Postscript listo";
$name_pdf_ready = "Archivo PDF listo";

# Common
$name_yes ="s�";
$name_no = "no";
$name_ok = "ok";
$name_cancel = "cancelar";

$message_about = "En caso de problemas\nlea doc.pdf :-)\no contacte con el autor en\nguitartex-users\@lists.sourceforge.net";

# Tool Tips
$balloon_new = "Crear un documento nuevo";
$balloon_open = "Abrir un documento existente";
$balloon_save = "Guardar el documento actual";
$balloon_latex = "Exportar a LaTeX";
$balloon_ps = "Exportar a postscript";
$balloon_pdf = "Exportar a PDF";
$balloon_transpose_up = "Transportar arriba";
$balloon_transpose_down = "Transportar abajo";
$balloon_highlight = "Coloreado de Sintaxis";
$balloon_chord = "Introducir acorde";
$balloon_guitartab = "Introducir tabulado de guitarra";
$balloon_basstab = "Introducir tabulado de bajo";
$balloon_fret = "Introduzca el n�mero del traste\ny haga click en el bot�n de cuerda\npara introducir notas en el tabulado";

# Templates
@name_template = ("empty", "Song", "Songbook");

