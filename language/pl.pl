# Menus
$name_menu_file = "Plik";
$name_menu_new = "Nowy";
$name_menu_open = "Otw�rz...";
$name_menu_save = "Zapisz";
$name_menu_saveas = "Zapisz jako...";
$name_menu_export = "Eksportuj";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Wyj�cie";

$name_menu_edit = "Edycja";
$name_menu_paste1 = "Wklej ze schowka";
$name_menu_insert = "Wstaw plik...";
$name_menu_crd = "crd -> chopro";
$name_menu_cut = "Wytnij: u�yj ctrl-x";
$name_menu_copy = "Kopiuj: u�yj ctrl-c";
$name_menu_paste2 = "Wklej: u�yj ctrl-v";

$name_menu_directives = "Dyrektywy";
$name_menu_startchorus = "Pocz�tek ch�ru";
$name_menu_endchorus = "Koniec ch�ru";
$name_menu_startbridge = "Pocz�tek mostu";
$name_menu_endbridge = "Koniec mostu";
$name_menu_starttab = "Pocz�tek tabulatury";
$name_menu_endtab = "Koniec tabulatury";
$name_menu_startinstr = "Pocz�tek cz�ci instrumentalnej";
$name_menu_endinstr = "Koniec cz�ci instrumentalnej";
$name_menu_guitartab = "Tabulatura gitarowa";
$name_menu_basstab = "Tabulatura basowa";
$name_menu_title = "Tytu�";
$name_menu_subtitle = "Podtytu�";
$name_menu_newpage = "Nowa strona";
$name_menu_comment = "Komentarz";
$name_menu_margin = "Marginesy";
$name_menu_second = "Drugi g�os";

$name_menu_songbook = "�pierwnik";
$name_menu_book = "Document class book";
$name_menu_booktitle = "Tytu� �piewnika";
$name_menu_bookauthor = "Autor �piewnika";
$name_menu_bookdate = "Data �piewnika";
$name_menu_chapter = "Rozdzia�";
$name_menu_include = "Do��cz";
$name_menu_even = "Parzyste";

$name_menu_color = "Kolory";
$name_menu_colorchorus = "Ch�r";
$name_menu_colorbridge = "Most";
$name_menu_colortab = "Tabulatura";
$name_menu_colorinstr = "Cz�� instrumentalna";
$name_menu_second = "Drugi g�os";
$name_menu_secondback = "Drugi g�os w tle";

$name_menu_transpose = "Transponowanie";
$name_menu_transup = "Do g�ry";
$name_menu_transdown = "Do do�u";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Pomoc";
$name_menu_about = "O programie";

#Icons
$name_icon_chords = "Akordy:";
$name_icon_tab = "Tabulatury:";
$name_icon_guitar = "Gitara";
$name_icon_bass = "Bas";
$name_icon_fret = " Pr�g:";
$name_icon_string = " Struna:";

$name_note_B = "B";

# File Dialogs
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Tekst";
$name_all_files = "Wszystkie pliki";
$name_ask_save = "Czy chcesz zapisa�\nbie��cy dokument?";
$name_tex_ready = "Plik TeX gotowy";
$name_ps_ready = "Plik Postscript gotowy";
$name_pdf_ready = "Plik PDF gotowy";

# Common
$name_yes ="tak";
$name_no = "nie";
$name_ok = "ok";
$name_cancel = "zrezygnuj";

$message_about = "W przypadku problem�w\nzagl�dnij do doc.pdf :-)\nlub skontaktuj si� z autorem\nguitartex-users\@lists.sourceforge.net";

# Tool Tips
$balloon_new = "Utw�rz nowy dokument";
$balloon_open = "Otw�rz istniej�cy dokument";
$balloon_save = "Zapisz bierz�cy dokument";
$balloon_latex = "Eksportuj do pliku LaTeX";
$balloon_ps = "Eksportuj do pliku postscript";
$balloon_pdf = "Eksportuj do pliku PDF";
$balloon_transpose_up = "Transponuj do g�ry";
$balloon_transpose_down = "Transponuj do do�u";
$balloon_highlight = "Pod�wietlanie sk�adni";
$balloon_chord = "Wstaw akord";
$balloon_guitartab = "Wstaw tabulatur� gitarow�";
$balloon_basstab = "Wstaw tabulatur� basow�";
$balloon_fret = "Wprowad� numer progu\ni naci�nij na przycisku odpowiedniej struny\naby wprowadzi� tabulatur�";

# Templates
@name_template = ("empty", "Song", "Songbook");
