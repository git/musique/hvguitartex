# Menus
$name_menu_file = "Arquivo";
$name_menu_new = "Novo";
$name_menu_open = "Abrir...";
$name_menu_save = "Salvar";
$name_menu_saveas = "Salvar como...";
$name_menu_export = "Exportar";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Sair";

$name_menu_edit = "Editar";
$name_menu_paste1 = "Colar do Clipboard";
$name_menu_insert = "Inserir arquivo...";
$name_menu_crd = "crd -> chopro";
$name_menu_cut = "Recortar: use ctrl-x";
$name_menu_copy = "Copiar: use ctrl-c";
$name_menu_paste2 = "Colar: use ctrl-v";

$name_menu_directives = "Diretivas";
$name_menu_startchorus = "Iniciar Chorus";
$name_menu_endchorus = "Terminar Chorus";
$name_menu_startbridge = "Iniciar Bridge";
$name_menu_endbridge = "Finalizar Bridge";
$name_menu_starttab = "Iniciar Tablatura";
$name_menu_endtab = "Finalizar Tablatura";
$name_menu_startinstr = "Iniciar Instrumental";
$name_menu_endinstr = "Finalizar Instrumental";
$name_menu_guitartab = "GuitarTab";
$name_menu_basstab = "BassTab";
$name_menu_title = "T�tulo";
$name_menu_subtitle = "Subt�tulo";
$name_menu_newpage = "Nova P�gina";
$name_menu_comment = "Coment�rio";
#TRADUZIR 'Margin Par'
$name_menu_margin = "Margem Par�grafo";
$name_menu_second = "Segunda Voz";

$name_menu_songbook = "Songbook";
$name_menu_book = "Document class book";
$name_menu_booktitle = "Book - t�tulo";
$name_menu_bookauthor = "Book - autor";
$name_menu_bookdate = "Book - data";
$name_menu_chapter = "Cap�tulo";
$name_menu_include = "Inclue";
$name_menu_even = "Par";

$name_menu_color = "Cores";
$name_menu_colorchorus = "Chorus";
$name_menu_colorbridge = "Bridge";
$name_menu_colortab = "Tablatura";
$name_menu_colorinstr = "Instrumental";
$name_menu_second = "Segunda Voz";
$name_menu_secondback = "Fundo segunda Voz";

$name_menu_transpose = "Transpor";
$name_menu_transup = "Acima";
$name_menu_transdown = "Abaixo";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Ajuda";
$name_menu_about = "sobre";

#Icons
#TRADUZIR TUDO
$name_icon_chords = "Acordes:";
$name_icon_tab = "Tablatura:";
$name_icon_guitar = "Guitar";
$name_icon_bass = "Bass";
$name_icon_fret = " Traste:";
$name_icon_string = " Corda:";

$name_note_B = "B";

# File Dialogs
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Texto";
$name_all_files = "Todos Arquivos";
$name_ask_save = "Deseja salvar o\ndocumento atual ?";
$name_tex_ready = "Arquivo TeX pronto";
$name_ps_ready = "Arquivo Postscript pronto";
$name_pdf_ready = "Arquivo PDF pronto";

# Common
$name_yes ="sim";
$name_no = "n�o";
$name_ok = "ok";
$name_cancel = "cancela";

$message_about = "Em caso de problemas\nleia doc.pdf :-)\nou contate o autor\nguitartex-users\@lists.sourceforge.net";

# Tool Tips
$balloon_new = "Iniciar novo documento";
$balloon_open = "Abrir documento existente";
$balloon_save = "Salvar documento atual";
$balloon_latex = "Exportar arquivo LaTeX";
$balloon_ps = "Exportar arquivo postscript";
$balloon_pdf = "Exportar arquivo PDF";
$balloon_transpose_up = "Transpor acima";
$balloon_transpose_down = "Transpor abaixo";
$balloon_highlight = "Sintaxe highlighting";
$balloon_chord = "Inserir acorde";
$balloon_guitartab = "Inserir guitar tablatura";
$balloon_basstab = "Inserir bass tablatura";
$balloon_fret = "Entre com n�mero do traste\ne clique bot�o da corda\npara entrar com as notas da tablatura";

# Templates
@name_template = ("vazio", "Song", "Songbook");
