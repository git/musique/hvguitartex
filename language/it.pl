# Menus
$name_menu_file = "File";
$name_menu_new = "Nuovo";
$name_menu_open = "Apri...";
$name_menu_save = "Salva";
$name_menu_saveas = "Salva con nome...";
$name_menu_export = "Esporta";
$name_menu_latex = "LaTeX";
$name_menu_postscript = "Postscript";
$name_menu_pdf = "PDF";
$name_menu_quit = "Esci";

$name_menu_edit = "Modifica";
$name_menu_paste1 = "Incolla dagli Appunti";
$name_menu_insert = "Inserisci file...";
$name_menu_crd = "crd -> chopro";
$name_menu_cut = "Taglia: usa ctrl-x";
$name_menu_copy = "Copia: usa ctrl-c";
$name_menu_paste2 = "Incolla: usa ctrl-v";

$name_menu_directives = "Direttive";
$name_menu_startchorus = "Inizio Ritornello";
$name_menu_endchorus = "Fine Ritornello";
$name_menu_startbridge = "Inizio Bridge";
$name_menu_endbridge = "Fine Bridge";
$name_menu_starttab = "Inizio Tablatura";
$name_menu_endtab = "Fine Tablatura";
$name_menu_startinstr = "Inizio Strumentale";
$name_menu_endinstr = "Fine Strumentale";
$name_menu_guitartab = "Tablatura per Chitarra";
$name_menu_basstab = "Tablatura per Basso";
$name_menu_title = "Titolo";
$name_menu_subtitle = "Sottotitolo";
$name_menu_newpage = "Nuova Pagina";
$name_menu_comment = "Commento";
$name_menu_margin = "Paragrafo a Margine";
$name_menu_second = "Seconda Voce";

$name_menu_songbook = "Canzoniere";
$name_menu_book = "Document class book";
$name_menu_booktitle = "Titolo";
$name_menu_bookauthor = "Autore";
$name_menu_bookdate = "Data";
$name_menu_chapter = "Capitolo";
$name_menu_include = "Includi file";
$name_menu_even = "Pagina Pari";

$name_menu_color = "Colori";
$name_menu_colorchorus = "Ritornello";
$name_menu_colorbridge = "Bridge";
$name_menu_colortab = "Tablatura";
$name_menu_colorinstr = "Strumentale";
$name_menu_second = "Seconda Voce";
$name_menu_secondback = "Seconda Voce in Sottofondo";

$name_menu_transpose = "Tonalità";
$name_menu_transup = "Alza";
$name_menu_transdown = "Abbassa";
$name_menu_transsharp = "b -> #";
$name_menu_transflat = "# -> b";
$name_menu_transgerman = "B -> H";
$name_menu_transinternational = "H -> B";

$name_menu_help = "Aiuto";
$name_menu_about = "Informazioni su ";

#Icons
$name_icon_chords = "Accordi:";
$name_icon_tab = "Tablatura:";
$name_icon_guitar = "Chitarra";
$name_icon_bass = "Basso";
$name_icon_fret = " Tasto:";
$name_icon_string = " Corda:";

$name_note_B = "Si";

# File Dialogs
$name_guitartex_files = "GuitarTeX";
$name_chordpro_files = "ChordPro";
$name_text_files = "Text";
$name_all_files = "Tutti i File";
$name_ask_save = "Vuoi salvare il\ndocumento corrente?";
$name_tex_ready = "File TeX pronto";
$name_ps_ready = "File Postscript pronto";
$name_pdf_ready = "File PDF pronto";

# Common
$name_yes ="Ś";
$name_no = "No";
$name_ok = "Ok";
$name_cancel = "Cancella";

$message_about = "In caso di problemi\nleggi doc.pdf :-)\noppure contatta l'autore\na jmiltz\@web.de";

# Tool Tips
$balloon_new = "Inizia un nuovo documento";
$balloon_open = "Apri un documento esistente";
$balloon_save = "Salva in documento corrente";
$balloon_latex = "Esporta come file LaTeX";
$balloon_ps = "Esporta come file postscript";
$balloon_pdf = "Esporta come file PDF";
$balloon_transpose_up = "Alza Tonalità";
$balloon_transpose_down = "Abbassa Tonalità";
$balloon_highlight = "Sintassi evidenziata";
$balloon_chord = "Inserisci accordo";
$balloon_guitartab = "Inserisci tablatura per chitarra";
$balloon_basstab = "Inserisci tablatura per basso";
$balloon_fret = "Inserisci il numero di tasto\ne premi sulla corda\nper inserire le note\nsulla tablatura";

# Templates
@name_template = ("Vuoto", "Canzone", "Canzoniere");

