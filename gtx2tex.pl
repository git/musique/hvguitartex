#!/usr/bin/perl

# The locale functionality is not usable prior to Perl 5.004
require 5.004;

# Enable to use UTF-8 in this source file
use utf8;

use File::Basename;
use Cwd;
use Getopt::Long;

# Import locale-handling tool set from POSIX module.
# This example uses: setlocale -- the function call
#                    LC_CTYPE -- explained below
use POSIX qw(locale_h);

setlocale(LC_CTYPE, "fr_FR.utf-8");

use locale;

# Configure STDOUT to output in UTF-8
binmode(STDOUT, ":utf8");

# Open all files in UTF-8 by default
use open ':encoding(utf8)';

GetOptions( "output=s" => \$output,
	    "lyrics" => \$lyrics,
	    "verbose" => \$verbose,
	    "italian" => \$italian,
	    "uppercase" => \$uppercase,
	    "bold" => \$bold,
	  );

$usage = "This is gtx2tex 2.8
usage: gtx2tex [--verbose] [--lyrics] [--output=ps|pdf] <file>
options:
--verbose        lets gtx2tex tell you what it is doing
--lyrics         print only lyrics
--output=ps|pdf  specify the output format; without the
                 output option a tex file is created
--italian        changes chord output to italian notation
                 (La, Si, Do, Re, Mi, Fa, Sol)
--bold           make all lyrics in bold
--uppercase      make all lyrics uppercase";

if ( ! $ARGV[0] ){
    die "$usage\n";
}

$verbose && print "This is gtx2tex\n";
$verbose && print "$output\n";

$libpath="/usr/local/lib/guitartex";

$song_size = "normalsize";
$new_song_init = "1";

# load configuration file
$home=$ENV{"HOME"};
$verbose && print "home=$home\n";
my $rcfile = "$home/.guitartexrc";
if (-e $rcfile) {
    do $rcfile or die "cannot load $rcfile";
}

$workdir = cwd;

$verbose && print "work directory: $workdir\n";

if ( $ARGV[0] =~ /^\// ){
    $filename = $ARGV[0];
}else{
    $filename = $workdir . "/" . $ARGV[0];
}

$outfilename = basename($filename);
$dirname = dirname($filename);
$outfilename =~ s/\.(.*?)$//;	# Suffix entfernen
$verbose && print "input file name is: $filename\n";
$verbose && print "output directory is: $dirname\n";
$verbose && print "output file name is: $outfilename\n";

chdir $dirname;

$env = "normal";
$space = "~";
$empty_line = "\n";
$new_page = "\\newpage\n";
$sharp = "\$\\sharp\$";
$flat = "\$\\flat\$";
$chorus_begin = "\\begin{chorus}\n";
$chorus_end = "\\end{chorus}\n";
$bridge_begin = "\\begin{bridge}\n";
$bridge_end = "\\end{bridge}\n";
$instr_begin = "\\begin{instr}\n";
$instr_end = "\\end{instr}\n";
$tab_begin = "\\begin{gtxtab}\n";
$tab_end = "\\end{gtxtab}\n";
$define = "";
$majeur_sept = "\$^\\Delta\$";
$mineur_maj7 = "m$majeur_sept";
$mineur = "m";
$mineur7 = "m\$^7\$";
$half_diminished = "m\$^{7\\flat5}\$";
$diminished = "\$^\\circ\$";
$diminished7 = "\$^{\\circ7}\$";

#load language file
do "/usr/local/lib/guitartex/language/$language.pl" or die "cannot load language
    file $language.pl\n";

# Leerzeichen im Dateinamen in Unterstriche ändern
$outfilename =~ s/\s/\_/g;

# open output file
open ( TEX, '>', "$outfilename.tex" ) || die "cannot open output file";

# LaTeX-Header ausgeben
latex_header();

$chaptercount = 0;

# Eingabedatei zeilenweise bearbeiten
while ( <CHORDPRO> ) {
    # Zeichen für Zeilenende abschneiden
    chomp;

    # Si la ligne commence par '{', c'est une directive
    if ( /^{/ ) {
        directive();
        next;
    }
    # Ligne vide
    if ( /^$/ ) {
        print TEX "$empty_line\n";
        next;
    }
    # Si la ligne commence par % ou # = commentaire
    if ( /^[%#]/ ) {
        next;
    }
    # Si la ligne commence par \ = commande latex
    if ( /\\.*/ ) {
        print TEX "$_\n";
        next;
    }
    # Si on est dans l'environnement tablature:
    if ( $env eq "tab" ) {
        print TEX "\\footnotesize \\verb!$_! \\normalsize \\newline\n";
        next;
    }

    $verbose && print "  chordpro data\n";

    if ($new_song_init) {
	# Augmente la taille des accords par rapport à la taille des paroles:
	$inc_song_size = $song_size;
	if ($song_size eq "footnotesize") { $inc_song_size = "small" }
	if ($song_size eq "small")        { $inc_song_size = "normalsize" }
	if ($song_size eq "normalsize")   { $inc_song_size = "large" }
	if ($song_size eq "large")        { $inc_song_size = "Large" }
	if ($song_size eq "Large")        { $inc_song_size = "LARGE" }
	if ($song_size eq "LARGE")        { $inc_song_size = "huge" }
	if ($song_size eq "huge")         { $inc_song_size = "Huge" }

        print TEX "\\renewcommand{\\gtxchordsize}{\\$inc_song_size}\n";

	$new_song_init = "";
    }

    crdpro();
}

# LaTeX-Footer ausgeben
latex_footer();

system ( "rm -f $outfilename.tmp" );

$output eq "ps" && create_ps();
$output eq "pdf" && create_pdf();


sub create_ps {
    $verbose && print "creating postscript\n";
    system ( "rm -f *.log *.aux *.ilg *.idx *.ind *.dvi *.toc *.mx1 *.mx2" );
    system ( "latex --interaction batchmode $outfilename" ) && die "error in first LaTeX run";
    if ( $document_class eq "book" ){
        system ( "makeindex $outfilename.idx" ) && die "error while creating index";
    }
    if ( $musixtex eq "yes" ){
        system ( "musixflx $outfilename" ) && die "error in musixflx run";
    }
    if ( $document_class eq "book" or $musixtex eq "yes" ){
        system ( "latex --interaction batchmode $outfilename" ) && die "error in second LaTeX run";
    }
    system ( "dvips $outfilename" ) && die "error while converting from dvi to postscript";
    system ( "rm -f *.log *.aux *.ilg *.idx *.ind *.dvi *.toc *.mx1 *.mx2 $outfilename.tex" );
}


sub create_pdf {
    $verbose && print "creating pdf\n";
    system ( "rm -f *.tpt *.log *.aux *.ilg *.idx *.ind *.dvi *.toc *.mx? *.mx2" );
    system ( "pdflatex $outfilename" ) && die "error while creating pdf output";
    if ( $document_class eq "book" ){
        system ( "makeindex $outfilename.idx" ) && die "error while creating index";
    }
    if ( $musixtex eq "yes" ){
        system ( "musixflx $outfilename" ) && die "error in musixflx run";
    }
    system ( "pdflatex $outfilename" ) && die "error while creating pdf output";
    system ( "thumbpdf $outfilename" ) && die "error while creating pdf thumbnails";
    system ( "pdflatex $outfilename" ) && die "error while creating pdf output";
    system ( "rm -f *.tpt *.log *.aux *.ilg *.idx *.ind *.dvi *.toc *.mx1 *.mx2 $outfilename.tex" );
}



sub crdpro { # Bearbeitung der Zeile aus der Eingabedatei
    if ( $lyrics ) {
        s/\[.*?\]//g; # Akkorde entfernen / On enlève les accords
        print TEX "$_\\\\\n" unless ( /^\s*$/ );
    } else {

	if ( $uppercase ) {
            $_ = uc($_); # Toutes les lettres en majuscule (incluant les accords).
        }
	else {
	    # Seulement les accords en majuscule
	    # .*? -> shortest match (non-greedy)
	    $_ =~ s/(\[.*?\])/${\(uc $1)}/g;
	}

        s/\s/$space/g; 				# Remplace une séquence de plusieurs espaces par un seul
        s/(\^)(.*?)(\])/\\\(^\{\2\}\\\)\]/g;	# Place le texte suivant '^' en exposant

        s/\[/\}\\gtxchord{/g;			# linke Klammer ersetzen / Remplace crochet gauche
        s/\]/}{/g;				# rechte Klammer ersetzen / Remplace crochet droit
        s/$/}/;  # Ajoute une accolade droite à la fin de la ligne
        s/^\}//; # Supprime l'accolade droite en début de ligne
        if ( $_ !~ /^\\/ ) {
            # Ajoute une élément \gtxchord sans accord: \gtxchord{}{TEXTE}
            s/^/\\gtxchord\{\}\{/
        }

        $verbose && print "    Étape1: $_\n";

        # Accord Majeur7
	s/(gtxchord\{[A-G][B#]?)MAJ7/\1$majeur_sept/g;

        # Accord suivi d'un bémol ou dièse optionel, suivi de 'sus' ou 'add' + 1 chiffre.
	s/(gtxchord\{[A-G][B#]?[0-9]?)SUS([0-9]+)/\1\$^{sus\2}\$/g;
	s/(gtxchord\{[A-G][B#]?[0-9]?)ADD([0-9]+)/\1\$^{add\2}\$/g;

        # Accord demi-diminué
        # Accord, suivi d'un bémol ou dièse optionel, suivi de 'm7b5'.
	s/(gtxchord\{[A-G][B#]?)M7B5/\1$half_diminished/g;

        # Accords diminués
        # On recherche le nom de l'accord, suivi d'un bémol ou dièse optionel, suivi de 'mb5'.
	s/(gtxchord\{[A-G][B#]?)MB5/\1$diminished/g;
	s/(gtxchord\{[A-G][B#]?)DIM7/\1$diminished7/g;
	s/(gtxchord\{[A-G][B#]?)DIM/\1$diminished/g;

        # Accords mineurs
	s/(gtxchord\{[A-G][B#]?)M7#([0-9]+)/\1m\$^{7\\sharp\2}\$/g; # Accord Am7#5
	s/(gtxchord\{[A-G][B#]?)M7/\1$mineur7/g;                 # Cm7
	s/(gtxchord\{[A-G][B#]?)M([0-9]+)/\1$mineur\$^{\2}\$/g;  # Cm13
	s/(gtxchord\{[A-G][B#]?)MMAJ7/\1$mineur_maj7/g;
	s/(gtxchord\{[A-G][B#]?)M/\1$mineur/g;                   # Cm (doit être en dernier)

        # Accord A7b5 ou A7b9
	s/(gtxchord\{[A-G][B#]?)([0-9])B([0-9])/\1\$^{\2\\flat\3}\$/g;

	# Accord A7#5 ou A7#9 ou A7#11
	s/(gtxchord\{[A-G][B#]?)([0-9])#([0-9]*)/\1\$^{\2\\sharp\3}\$/g;

        # Accord A7 ou A6 ou Ab6 ou Ab13
	s/(gtxchord\{[A-G][B#]?)([0-9]*)/\1\$^{\2}\$/g;

	s/(gtxchord\{[A-G])B/\1$flat/g;   # Remplace 'gtxchord{AB'  par 'gtxchord{Ab'

	s/(\/[A-G])B/\1$flat/g;               # Remplace '/AB'          par '/Ab'

        s/#/$sharp/g;     # Remplace le symbole '#' par le symbole diese

        if ( $italian ) {
            s/gtxchord\{D/gtxchord{Re/g;
            s/gtxchord\{E/gtxchord{Mi/g;
            s/gtxchord\{F/gtxchord{Fa/g;
            s/gtxchord\{G/gtxchord{Sol/g;
            s/gtxchord\{A/gtxchord{La/g;
            s/gtxchord\{B/gtxchord{Si/g;
            s/gtxchord\{H/gtxchord{Si/g;
            s/gtxchord\{C/gtxchord{Do/g;

            s!/D!/Re!g;
            s!/E!/Mi!g;
            s!/F!/Fa!g;
            s!/G!/Sol!g;
            s!/A!/La!g;
            s!/B!/Si!g;
            s!/H!/Si!g;
            s!/C!/Do!g;
        }
        print TEX "$_\\\\\n";
    }
}


sub directive{
    if 	( /{soc}|{start_of_chorus}/ ) 	{ $env = "chorus"; print TEX "$chorus_begin";
    }elsif	( /{eoc}|{end_of_chorus/ ) 	{ $env = "normal"; print TEX "$chorus_end";
    }elsif	( /{sob}|{start_of_bridge}/ ) 	{ $env = "bridge"; print TEX "$bridge_begin";
    }elsif	( /{eob}|{end_of_bridge/ ) 	{ $env = "normal"; print TEX "$bridge_end";
    }elsif	( /{soi}|{start_of_instr}/ ) {
        $env = "instr";
        print TEX "$instr_begin" unless ( $lyrics );
    }elsif ( /{eoi}|{end_of_instr/ ) {
        $env = "normal";
        print TEX "$instr_end" unless ( $lyrics );
    }elsif ( /{sot}|{start_of_tab}/ ) {
        $env = "tab";
        print TEX "$tab_begin" unless ( $lyrics );
    }elsif ( /{eot}|{end_of_tab/ ) {
        $env = "normal";
        print TEX "$tab_end" unless ( $lyrics );
    }elsif ( /{np}/ ) {
        print TEX "$new_page";
        print TEX "\\hfill\\break";
    }elsif ( /{even}/ ) {
        print TEX "\\ifthenelse{\\isodd{\\thepage}}{}{\\newpage ~}";
    }elsif ( /{title:|{t:/ ) {
        s/(\{.*:)(.+)(\})/\2/;
        if ( $document_class eq "book" ){
            print TEX "$define" unless ( $lyrics );
        }
        print TEX "%\n%----- New Song -----\n";
        print TEX "\\newpage\\addcontentsline{toc}{section}{$_}\n";

        if ( $bold ) {
            print TEX "\\bfseries\n";
        }

        # Réinitialisation à la fonte normale
        print TEX "\\normalsize\n";
	$song_size = "normalsize";
	$new_song_init = "1";

        if ( $output eq "pdf" ) { print TEX "\\pdfdest name {$_} fith \\pdfoutline goto name {$_} {$_}" }

        $song_title = $_;
        $song_title_lc = $song_title;
        if ( $uppercase ) {
            $song_title = uc($song_title);
        }

        # \titlespacing{command}{left}{before-sep}{after-sep}[right-sep]
        print TEX "\\titlespacing{\\section}{0em}{0em}{-0.6cm}\n";

        print TEX "\\begin{center} \\section*{$song_title}\\end{center}\n";
        $define="";
        $verbose && print TEX "\\typeout{$song_title}";
    }elsif ( /{subtitle:|{st:/ ) {
        s/(\{.*:)(.+)(\})/\2/;

        print TEX "\\index{$_!$song_title_lc}";
        print TEX "\\normalfont\n";
        print TEX "\\footnotesize\n";
        print TEX "\\begin{center}$_\\end{center}\n";
        print TEX "\\normalsize\n";

        if ( $bold ) {
            print TEX "\\bfseries\n";
        }
        else
        {
            print TEX "\\normalfont\n";
        }
    }elsif ( /{margin:|{m:/ ) {
        s/(\{.*:)(.+)(\})/\2/;
        print TEX "\\marginpar{$_}\n";
    }elsif ( /{comment:|{c:/ ) {
        s/(\{.*:)(.+)(\})/\2/;
        print TEX "\\textit{$_}\\\\\n" unless ( $lyrics );
    }elsif ( /{second:|{2:/ ) {
        s/(\{.*:)(.+)(\})/\2/;
        s/\s/$space/g;	#Leerzeichen durch feste Leerzeichen ersetzen
        print TEX "\\colorbox{second_back}{\\textcolor{second_text}{$_}}\\\\\n" unless ( $lyrics );
    }elsif ( /{chapter:/ ) {
        $chaptercount++;
        s/(\{.*:)(.+)(\})/\2/;
        if ( $document_class eq "book" ) {
            print TEX "$define" unless ( $lyrics );
            $define = "";
            print TEX "\n\\chapter{$_} \\fancyhead[CO]{$_}\n\n";
            if ( $output eq "pdf" ) {
                $count = $titlecount[$chaptercount];
                $verbose && print TEX "% chapter: $_, $count entries\n";
                print TEX "\\pdfdest name {$_} fith \\pdfoutline goto name {$_} count $count {$_}"
            }
            $verbose && print TEX "\\typeout{$_}";
        };
    }elsif ( /{define/ ) {
        s/\{//;
        s/\}//;
        s/define//;
        s/://;
        s/base-fret//;
        s/frets//;
        s/[Xx]/-1/g;
        s/- /-1/g;
        s/-$/-1/g;
        s/-1/x/g;
        @def = split(" ",$_);
        @def[0] =~ s/#/\\#/g;
        #$define .= "\\mbox{\\begin{picture}(20,18)\\special{\"0 0 moveto ";
        #$define .= "($def[0]) $def[2] $def[3] $def[4] $def[5] $def[6] $def[7] $def[1]";
        #$define .= " 0 dots}\\end{picture}}\n";
        $define .= " ~\\gchord{{$def[1]}}{$def[2],$def[3],$def[4],$def[5],$def[6],$def[7]}{$def[0]}";
    }elsif ( /{guitartab:/ ) {
        print TEX "\\guitartab\n" unless ( $lyrics );
        newtab() unless ( $lyrics );
    }elsif ( /{basstab:/ ) {
        print TEX "\\basstab\n" unless ( $lyrics );
        newtab() unless ( $lyrics );
    }elsif ( /{size:/ ) {
        s/(\{.*:)(.+)(\})/\2/;
        print TEX "\\$_\n";
	$song_size = $_;
	$verbose && print "Song size: $song_size\n";
    }
}


sub newtab {
    if ( /{guitartab:/ ) { $guitartab="yes" } else { $guitartab ="no" };
    s/(\{.*:)(.+)(\})/\2/;
    s/\]\[/\]~~~\[/g;       #fill in fixed spaces between notes
    s/\_/~~~/g;             #fill in extra space
    s/\s/~/g;               #replace spaces with fixed spaced
    s/\]&\[/\]\[/g;         #chord
    s/\[1;/\[2.5ex;/g;
    s/\[2;/\[4.5ex;/g;
    s/\[3;/\[6.5ex;/g;
    s/\[4;/\[8.5ex;/g;
    s/\[5;/\[10.5ex;/g;
    s/\[6;/\[12.5ex;/g;
    if ( $guitartab eq "yes") {
        s/\|/~~~\[3.7ex;\|\][5.7ex;\|\][7.7ex;\|\][9.7ex;\|\][11.3ex;\|\]~~~/g;
    }else{
        s/\|/~~~\[3.7ex;\|\][5.7ex;\|\][7.3ex;\|\]~~~/g;
    }
    s/\[/\\gtxtabs{/g;
    s/\]/\}/g;
    s/;/\}\{/g;
    print TEX "$_\n\n";
}

#-------------------------------------------------------------------------------
# subroutine latex_header
#
# write the start of the LaTeX file
#-------------------------------------------------------------------------------


sub latex_header {

    includes();

    $document_class = "article";			#default document class
    if ( $output eq "ps" ){$geometry = "dvips, "}	#default geometry parameter
    $preamble = "";					#default preamble parameter
    $musixtex = "no";
    $musixlyr = "no";
    $tabdefs = "no";
    $chaptercount = 0;
    @titlecount = (0);

    open ( CHORDPRO, '<', "$dirname/$outfilename.tmp" ) || die "cannot open input file";

    # read the whole document for settings
    while ( <CHORDPRO> ){
        if ( /^{/ ) {	# nur Zeilen mit Direktiven bearbeiten
            chomp;
            if ( /{book_title:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $title = $_ ;
            }elsif ( /{book_author:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $author = $_ ;
            }elsif ( /{book_date:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $date = $_ ;
            }elsif ( /{document_class:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $document_class = $_ ;
                $verbose && print "Document_class: $document_class\n";
                if ( $document_class ne "book" && $document_class ne "article" ) {
                    die "parameter of directive document_class must be \"book\" or \"article\"\n";
                }
            }elsif ( /{font_size:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $font_size = $_ ;
            }elsif ( /{color_chorus:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_chorus = $_ ;
            }elsif ( /{color_bridge:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_bridge = $_ ;
            }elsif ( /{color_instr:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_instr = $_ ;
            }elsif ( /{color_tab:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_tab = $_ ;
            }elsif ( /{color_second:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_second = $_ ;
            }elsif ( /{color_second_back:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $color_second_back = $_ ;
            }elsif ( /{geometry:|{g:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $geometry .= "$_," ;
            }elsif ( /{preamble:|{p:/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $preamble .= "$_\n" ;
            }elsif ( /{musixtex}/ ) {
                $musixtex = "yes";
            }elsif ( /{musixlyr}/ ) {
                $musixlyr = "yes";
            }elsif ( /{tabdefs}/ ) {
                $tabdefs = "yes";
            }elsif ( /{chapter/ ) {
                s/(\{.*:)(.+)(\})/\2/;
                $chaptercount++;
                push @titlecount, (0);
            }elsif ( /{title:|{t:/ ) {
                $titlecount[$chaptercount]++;
            }
        }
    }

    $verbose && print "$chaptercount chapters, @titlecount\n";

    chop $geometry; 			#delete last comma
    close ( CHORDPRO );

    #open temporary file
    open ( CHORDPRO, '<', "$dirname/$outfilename.tmp" ) || die "cannot open input file";

    #print preamble into LaTeX file
    if ( $output eq "pdf" ) { print TEX "\\pdfoutput=1
                                                    \\pdfcompresslevel=9
                                                    \\pdfpageheight=29.7cm
                                                    \\pdfpagewidth=21cm
                                                    \\pdfcatalog  {/PageMode /UseOutlines}
                              \\pdfinfo {
                              /Title ($title)
                                  /Author ($author)
                                  /CreationDate ($date)
                                  /Producer (GuitarTeX by Joachim Miltz)
                                                }
                              ";
    }


    print TEX "\\documentclass[$languages{$language},";

    if ( $output eq "pdf" ) { print TEX " pdftex," }

    print TEX " $font_size"."pt]{$document_class}
    \\usepackage[$geometry]{geometry}
    \\usepackage[T1]{fontenc}
    ";

    if ( $language eq "pl" ){
        print TEX "\\usepackage[latin2]{inputenc}\n"
    }else{
        print TEX "\\usepackage[utf8]{inputenc}\n"
    }

    print TEX "\\usepackage{babel}
    \\usepackage{color}
    \\usepackage{graphicx}
    \\usepackage{ifthen}
    \\usepackage{calc}
    %
                                                    ";

    if ( $output eq "pdf" ){
        print TEX "\\usepackage[pdftex]{thumbpdf}\n";
    }

    if ( $musixtex eq "yes" ){
        print TEX "
\\input{musixtex}
\\input{musixlyr}
\\input{tabdefs}
"
    }

    print TEX "
        \\usepackage{setspace}
        \\usepackage{titlesec}
    ";

    if ( $document_class eq "book" ){
        print TEX "
        \\title{$title}
        \\author{$author}
        \\date{$date}
        \\usepackage{makeidx}
        \\makeindex
        \\usepackage{fancyhdr}
        \\pagestyle{myheadings}

        %%\\titleformat{command}[shape{format}{label}{sep}{before-code}[after-code]
        \\titleformat{\\section}{\\centering}{}{0em}{\\bf\\large}
        "
    }else{
        print TEX "
\\pagestyle{empty}
"};

    print TEX "\\usepackage{gchords}
    ";

    print TEX "
                                                    %
                                                    %------definition of font colors-----------
                                                    \\definecolor{chorus}{rgb}{$color_chorus}
    \\definecolor{bridge}{rgb}{$color_bridge}
    \\definecolor{instr}{rgb}{$color_instr}
    \\definecolor{tab}{rgb}{$color_tab}
    \\definecolor{second_text}{rgb}{$color_second}
    \\definecolor{second_back}{rgb}{$color_second_back}
    %
                                                    %------definition of environments-------
                                                    \\newenvironment{chorus}{\\color{chorus}}{\\normalcolor}
    \\newenvironment{bridge}{\\color{bridge}}{\\normalcolor}
    \\newenvironment{instr}{\\color{instr}}{\\normalcolor}
    \\newenvironment{gtxtab}{\\color{tab}}{\\normalcolor}
    %
                                                    %------counter for line numbers------
                                                    %\\newcounter{verscnt}[section]
                                                    %\\newcommand{\\vers}{\\stepcounter{verscnt}%
                                                                              %\\makebox[0.5cm][r]{\\scriptsize\\theverscnt\\normalsize}%
                                                                              %\\makebox[0.5cm]{}}
    %
                                                    \\newcommand{\\gtxchordsize}{\\$song_size}
    \\setlength{\\parindent}{0pt}

    % Modification pour augmenter la taille des accords.
                                                    \\onehalfspacing
                                                    %%\\doublespacing
                                                    \\setstretch{1.8}

    %------PostScript-Header für die Chord-Songs------
                                                    %\\special{header=$libpath/chord-common.ps}

    \\sloppy
                                                    \\setlength{\\unitlength}{1mm}

    \\newcommand{\\gtxchord}[2]{\\sbox{\\gtxchordbox}{#1}\\sbox{\\textbox}{#2}%
    \\ifthenelse{\\lengthtest{\\wd\\textbox>\\wd\\gtxchordbox}}%
    {%
         % Boîte pour les accords (plus gros)
         \\raisebox{2.5ex}[2ex][2.5ex]{\\makebox[0pt][l]{\\gtxchordsize\\bf#1}}#2%
                                       }{%
                                             % Boîte pour les accords (plus gros)
                                             \\raisebox{2.5ex}[2ex][2.5ex]{\\makebox[0pt][l]{\\gtxchordsize\\bf#1}}\\makebox[\\wd\\gtxchordbox+0.5em][l]{#2}%
                                                                           }%
                                         }%

                                             \\newcommand{\\guitartab}{%
                                                                           \\makebox[0cm][l]{\\raisebox{12.5ex}{\\footnotesize{e}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{10.5ex}{\\footnotesize{B}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{8.5ex}{\\footnotesize{G}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{6.5ex}{\\footnotesize{D}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{4.5ex}{\\footnotesize{A}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{2.5ex}{\\footnotesize{E}}}%
                                                                           ~~
                                                                           \\makebox[0cm][l]{\\raisebox{13ex}{\\line(1,0){130}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{11ex}{\\line(1,0){130}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{9ex}{\\line(1,0){130}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{7ex}{\\line(1,0){130}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{5ex}{\\line(1,0){130}}}%
                                                                           \\makebox[0cm][l]{\\raisebox{3ex}{\\line(1,0){130}}}%
                                                                           ~}

    \\newcommand{\\basstab}{%
                                \\makebox[0cm][l]{\\raisebox{8.5ex}{\\footnotesize{G}}}%
                                \\makebox[0cm][l]{\\raisebox{6.5ex}{\\footnotesize{D}}}%
                                \\makebox[0cm][l]{\\raisebox{4.5ex}{\\footnotesize{A}}}%
                                \\makebox[0cm][l]{\\raisebox{2.5ex}{\\footnotesize{E}}}%
                                ~~
                                \\makebox[0cm][l]{\\raisebox{9ex}{\\line(1,0){130}}}%
                                \\makebox[0cm][l]{\\raisebox{7ex}{\\line(1,0){130}}}%
                                \\makebox[0cm][l]{\\raisebox{5ex}{\\line(1,0){130}}}%
                                \\makebox[0cm][l]{\\raisebox{3ex}{\\line(1,0){130}}}%
                                ~}

    \\newcommand{\\gtxtabs}[2]{\\makebox[0cm][l]{\\raisebox{#1}{#2}}}

    $preamble

        %------start of document-----------
        \\begin{document}
    \\newsavebox{\\gtxchordbox}
    \\newsavebox{\\textbox}
    ";

    if ( $document_class eq "book" ){
        print TEX "
\\fancyhead
\\fancyfoot
\\frontmatter
\\maketitle
\\tableofcontents
\\mainmatter
\\fancyhead[CE]{$title}
\\fancyhead[LE,RO]{\\thepage}
"};

    print TEX "%\n%\n";
}


#-------------------------------------------------------------------------------
# subroutine latex_footer
#
# write the end of the LaTeX file
#-------------------------------------------------------------------------------

sub latex_footer {
    print TEX "$define\n";
    if ( $document_class eq "book") {
        print TEX "\\backmatter\n";
        print TEX "\\footnotesize\n";
        print TEX "\\printindex\n";
    }
    print TEX "\\end{document}\n";
    close ( TEX );
}

#-------------------------------------------------------------------------------
# subroutine includes
#
# look for includes, save everything into a temporary file
#-------------------------------------------------------------------------------

sub includes{
    open ( TMP, '>', "$dirname/$outfilename.tmp" ) || die "cannot open temporary output file";

    # open input file
    open ( CHORDPRO, '<', "$filename" ) || die "cannot open input file";

    while ( <CHORDPRO> ){
        if ( /{include:/ ) {		#read only lines with an include directive
            chomp;  # avoid \n on last field
            s/(\{.*:)(.+)(\})/\2/;
            print TMP "#$_\n#\n";

            open ( INSERT, '<', "$dirname/$_" ) || die "cannot open include file: $_";

            while ( <INSERT> ){	#insert file to include
                print TMP;
            }
        }else{
            print TMP;
        }
        close (INSERT);
    }
    close ( CHORDPRO );
    close ( TMP );
}
