PREFIX ?= /usr/local

conf:
	install -m 644 guitartex.conf ${HOME}/.guitartexrc

install:
	install -m 755 gtx2tex.pl $(PREFIX)/bin/gtx2tex
	install -m 755 guitartex.pl $(PREFIX)/bin/guitartex
	mkdir -p $(PREFIX)/lib/guitartex
	cp --recursive --force * $(PREFIX)/lib/guitartex

uninstall:
	rm $(PREFIX)/bin/guitartex
	rm $(PREFIX)/bin/gtx2tex
	rm -r $(PREFIX)/lib/guitartex
